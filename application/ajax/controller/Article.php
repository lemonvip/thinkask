<?php
/*
+--------------------------------------------------------------------------
|   WeCenter [#免费开发#]
|   ========================================
|   by Jerry
|   http://www.5ihelp.com
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\ajax\controller;
use app\ajax\model\User as askuser;
use app\ajax\model\Article as Articlea;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
class Article extends Controller
{
  protected $request;
	public function _initialize()
    {
        $this->request = Request::instance();
        //用户是否登陆
       if(Session::get('thinkask_uid')<0){
        return ;
       }
       

    }
    public function edit(){
      //表判断
       $table =$this->request->only(['table']);
       //防注入,影响其它表
       if(decode($table['table'])!="article"){
        return ;
       }
       //字段判断
       if($title = $this->request->only(['title'])){
            if(empty($title['title'])){
                $this->error('标题不能为空');
            }
       }
        if($category_id = $this->request->only(['category_id'])){
            if(empty($category_id['category_id'])||$category_id['category_id']<1){
                $this->error('请选择分类');
            }
       }
       //内容
       if($message = $this->request->only(['message'])){
        //文章内容必填但问题不需要
            if(empty($message['message'])){
                $this->error('内容不能为空');
            }
       }

      $id = model('Article')->edit($this->request->param());
      if($id){
        $this->success('操作成功','','/aricle/id/'.$id);
      }



    }


}
