<?php
namespace app\index\controller;
use app\common\model\Article as Article;
use app\index\model\Index as Indexss;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
use think\Hook;

class index extends Controller
{

	public function _initialize()
    {
        // $this->view = new \think\View();
        // $this->article = new app\index\model();
    }
    public function index()
    {
    	$this->assign('list',Article::getArList());
        if((int)Session::get('thinkask_uid')>0){
            $this->assign('uid',Session::get('thinkask_uid'));
            $userinfo = model('users')->getUserByUid(Session::get('thinkask_uid'));
            // show($userinfo);
            $this->assign('userinfo',$userinfo);
        }else{
            $this->assign('uid',0);
        }
		return $this->fetch();
    }

}
