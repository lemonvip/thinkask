<?php
namespace app\index\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
use think\Hook;

class Question extends Controller
{
    protected $request;
    public function _initialize()
    {
     if((int)Session::get('thinkask_uid')>0){
                $this->assign('uid',Session::get('thinkask_uid'));
                $userinfo = model('users')->getUserByUid(Session::get('thinkask_uid'));
                // show($userinfo);
                $this->assign('userinfo',$userinfo);
            }else{
                $this->assign('uid',0);
            }
            // show($userinfo);
            $this->request = Request::instance();

    }
    public function index()
    {
        $id = $this->request->only(['id']);
        if($this->request->param()==""||$id['id']<1){
            $this->error('参数错误','/');
        }
        //最新
        $this->assign($re = model('Article')->getArById($id['id']));
        // show($re);
        return $this->fetch('index/article');
    }

}
