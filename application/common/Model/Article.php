<?php
/*
+--------------------------------------------------------------------------
|   WeCenter [#免费开发#]
|   ========================================
|   by Jerry
|   http://www.5ihelp.com
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\model;
use think\Model;
use think\Db;
class article extends Model
{  
    protected $request;
    // protected $pk = 'uid';
    // protected $table = 'article';
    static public function getArList()
    {
    	
    return $list = Db::name('article')->select();

    }
    public function edit($data){
    	$data['message'] = htmlspecialchars($data['message']);
    	$data['title'] = htmlspecialchars($data['title']);
    	$data['article_id']  = (int)$data['article_id'];
    	if($data['article_id']>0){
    		return $this->publish($data);
    	}else{
    		return $this->add($data);
    	}
    }
    private function add($data){
    	return  Db::name('article')->insertGetId($data);
    }
    private function publish($data){

    }
    public function getArById($id){
        $join = [
                    ['aws_users us','a.uid=us.uid'],
                    // ['think_card c','a.card_id=c.id'],
                ];
        return Db::name('article')->alias('a')->join($join)->where('a.id',$id)->find();
    }


}