<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:39:"../template/5ihelp/publish\article.html";i:1478246698;s:37:"../template/5ihelp/public\header.html";i:1478246853;s:37:"../template/5ihelp/public\footer.html";i:1478230408;}*/ ?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta name="renderer" content="webkit" />
<title>5ihelp</title>
<meta name="keywords" content="" />
<meta name="description" content=""  />
<!-- <base href="http://git.cooldreamer.com/ask/?/" /> -->
<!-- <link href="http://git.cooldreamer.com/ask/static/css/default/img/favicon.ico?v=20160523" rel="shortcut icon" type="image/x-icon" /> -->

<link rel="stylesheet" type="text/css" href="/static/5iask/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/static/5iask/css/icon.css" />
<link href="/static/5iask/css/default/common.css?v=20160523" rel="stylesheet" type="text/css" />
<link href="/static/5iask/css/default/link.css?v=20160523" rel="stylesheet" type="text/css" />
<link href="/static/5iask/js/plug_module/style.css?v=20160523" rel="stylesheet" type="text/css" />
<script src="/static/5iask/js/jquery.2.js?v=20160523" type="text/javascript"></script>
<script src="/static/5iask/js/jquery.form.js?v=20160523" type="text/javascript"></script>


 <script src="/static/plus/js/plugins/layer/layer.js"></script>
   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    // layer.config(
    //     extend: ['skin/moon/style.css'], //加载新皮肤
    //     skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    // );

    </script>
<!--[if lte IE 8]>
	<script type="text/javascript" src="/static/5iask/js/respond.js"></script>
<![endif]-->
</head>

<body>
	<div class="aw-top-menu-wrap">
		<div class="container">
			<!-- logo -->
			<div class="aw-logo hidden-xs">
				<a href="/"></a>
			</div>
			<!-- end logo -->
			<!-- 搜索框 -->
			<div class="aw-search-box  hidden-xs hidden-sm">
				<form class="navbar-search" action="search/" id="global_search_form" method="post">
					<input class="form-control search-query" type="text" placeholder="搜索问题、话题或人" autocomplete="off" name="q" id="aw-search-query" />
					<span title="搜索" id="global_search_btns" onClick="$('#global_search_form').submit();"><i class="icon icon-search"></i></span>
					<div class="aw-dropdown">
						<div class="mod-body">
							<p class="title">输入关键字进行搜索</p>
							<ul class="aw-dropdown-list collapse"></ul>
							<p class="search"><span>搜索:</span><a onClick="$('#global_search_form').submit();"></a></p>
						</div>
						<div class="mod-footer" style="display:none;">
							<a href="" onClick="$('#header_publish').click();" class="pull-right btn btn-mini btn-success publish"><?php // // _e('发起问题'); ?></a>
						</div>
					</div>
				</form>
			</div>
			<!-- end 搜索框 -->
			<!-- 导航 -->
			<div class="aw-top-nav navbar">
				<div class="navbar-header">
				  <button  class="navbar-toggle pull-left">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
				  <ul class="nav navbar-nav">
				  <!-- <li><a href="/" ><i class="icon icon-index"></i> 首页</a></li> -->

					<?php  if($uid>0){ ?>
						<!-- <li><a href="home/"><i class="icon icon-home"></i> 动态</a></li> -->
					<?php }  ?>
					<!-- <li><a href="" class=""><i class="icon icon-list"></i> 发现</a></li> -->
					<li><a href="question/" class="">问题</a></li>
					<li><a href="<?php echo url('index/article'); ?>" class="">文章</a></li>
					<!-- <li><a href="topic/"><i class="icon icon-topic"></i>话题</a></li> -->
					<?php  if ($uid>0){  ?>
					<li style="display:none;">
						<a href="notifications/" class=""><i class="icon icon-bell"></i> 通知</a>
						<span class="badge badge-important" style="display:none" id="notifications_unread"><?php // // echo $this->user_info['notification_unread']; ?></span>
						<div class="aw-dropdown pull-right hidden-xs">
							<div class="mod-body">
								<ul id="header_notification_list"></ul>
							</div>
							<div class="mod-footer">
								<a href="notifications/">查看全部</a>
							</div>
						</div>
					</li>
					<?php }   ?>
					<!-- <li><a href="help/"> <i class="icon icon-bulb"></i> 帮助</a></li> -->
					<li>
						<a style="font-weight:bold;">· · ·</a>
						<!-- <div class="dropdown-list pull-right">
							<ul id="extensions-nav-list">
								<li><a href="ticket/"><i class="icon icon-order"></i> 工单</a></li>
								<li><a href="project/"><i class="icon icon-activity"></i> 活动</a></li>
							</ul>
						</div> -->
					</li>
				  </ul>
				</nav>
			</div>
			<!-- end 导航 -->
			<!-- 用户栏 -->
			<div class="aw-user-nav">
				<!-- 登陆&注册栏 -->
				<?php  if($uid>0){ ?>
					<a href="people/" style="" class="aw-user-nav-dropdown">
						<img alt="<?php echo $userinfo['user_name']; ?>" src="/uploads/avatar/<?php echo $userinfo['avatar_file']; ?>" />
							<span class="badge badge-important"></span>
					</a>
					<div class="aw-dropdown dropdown-list pull-right" style="">
						<ul class="aw-dropdown-list">
							<li><a href="inbox/"><i class="icon icon-inbox"></i> 私信<span class="badge badge-important collapse" id="inbox_unread">0</span></a></li>
							<li class="hidden-xs"><a href="account/setting/profile/"><i class="icon icon-setting"></i> 设置</a></li>
							<li class="hidden-xs"><a href="admin/"><i class="icon icon-job"></i> 管理</a></li>
							<li><a href="account/logout/"><i class="icon icon-logout"></i></a></li>
						</ul>
					</div>
				<?php  }else{ ?>
					<a class="login btn btn-normal btn-primary" href="<?php echo url('ucenter/user/login'); ?>">登录</a>
					<a class="register btn btn-normal btn-success" href="/ucenter/user/reg.html">注册</a>
				<?php }  ?>	
				<!-- end 登陆&注册栏 -->
			</div>
			<!-- end 用户栏 -->
			<!-- 发起 -->
			<div class="aw-publish-btn" style="">
				<a id="header_publish" class="btn-primary"href="publish/"><i class="icon icon-ask"></i>发起</a>
				<div class="dropdown-list pull-right">
					<ul>
						<li>
							
								
								<a href="<?php echo url('post/publish/article'); ?>">问题</a>
					
						</li>
						<li>
							
								<a onclick="<?php echo url('post/publish/question'); ?>">文章</a>
						
						</li>
				<!-- <li><a href="ticket/publish/">工单</a></li>
						<li><a href="project/publish/">活动</a></li> -->
				
					</ul>
				</div>
			</div>
			<!-- end 发起 -->
		</div>
	</div>

	<div class="aw-email-verify" style="display:none;">
		<div class="container text-center">
			<a onclick="AWS.ajax_request(G_BASE_URL + '/account/ajax/send_valid_mail/');"></a>
		</div>
	</div>


<div class="aw-container-wrap">
	<div class="container aw-publish aw-publish-article">
		<div class="row">
			<div class="aw-content-wrap clearfix">
				<div class="col-sm-12 col-md-9 aw-main-content">
					<!-- tab 切换 -->
					<ul class="nav nav-tabs aw-nav-tabs active">
						<li class="active"><a href="<?php echo url('post/publish/article'); ?>">文章</a></li>
						
						<li><a href="<?php echo url('post/publish/question'); ?>">问题</a></li>
						
						<h2 class="hidden-xs"><i class="icon icon-ask"></i> 文章</h2>
					</ul>
					<!-- end tab 切换 -->
					<form action="<?php echo url('ajax/article/edit'); ?>" method="post" id="question_form" class="cmzForm">
						<input type="hidden" name="post_hash" value="" />
						<input type="hidden" name="attach_access_key" value="" />
						<input type="hidden" name="article_id" id="article_id" value="" />
						<div class="aw-mod aw-mod-publish">
							<div class="mod-body">
								<h3>文章标题:</h3>
								<!-- 文章标题 -->
								<div class="aw-publish-title">
									<input type="text" name="title" value="" class="form-control" />
								
									<div class="aw-dropdown aw-question-dropdown">
										<i class="aw-icon i-dropdown-triangle active"></i>
										<p class="title">没有找到相关结果</p>
										<ul class="aw-question-dropdown-list"></ul>
									</div>
								<div class="dropdown">
										<select name="category_id" id="" class="form-control">
												<option value="">选择分类</option>
												<?php foreach ($category as  $v): ?>
												<option value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>
											<?php endforeach ?>
											</select>
									
								
								</div>
								<!-- end 文章标题 -->

								<!-- 指定分组  -By Jerry 2016.11.1-->
								<h3>指定分组:</h3>
								
								<div class="aw-publish-title">
									<select name="power_group" id="" class="form-control">
									<option value="0">不指定用户组</option>
									<?php if(is_array($group) || $group instanceof \think\Collection): $i = 0; $__LIST__ = $group;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
										<option value="<?php echo $v['group_id']; ?>"><?php echo $v['group_name']; ?></option>
									<?php endforeach; endif; else: echo "" ;endif; ?>
										
									</select>
									
								</div>
								
									<!-- end 指定分组  -By Jerry 2016.11.1-->

								<h3>文章内容:</h3>
								<div class="aw-mod aw-editor-box">
									<div class="mod-head">
										<div class="wmd-panel">
					                        <script type="text/javascript" src="/static/plus/js/plugins/ueditor/ueditor.config.js"></script>
					                        <script type="text/javascript" src="/static/plus/js/plugins/ueditor/ueditor.all.js"></script>
					                        <script type="text/javascript" src="/static/plus/js/plugins/ueditor/ueditor.parse.js"></script>
					                        <textarea name="message" class="message-baiduedit"  style="display:none;"></textarea>
					                         <script id="message" type="text/plain" style="300"></script>
					                        <script type="text/javascript">
					                            var ue = UE.getEditor("message");
					                            ue.addListener( "selectionchange", function () {
					                                $(".message-baiduedit").val(ue.getContent())
					                               
					                            } );
					                            
					                       </script>
								        </div>
									</div>
									<div class="mod-body">
										<p class="text-color-999"><span class="pull-right" id="question_detail_message">&nbsp;</span></p>
									<!-- 	
										<div class="aw-upload-box">
											<a class="btn btn-default">上传附件</a>
											<div class="upload-container"></div>
											<span class="text-color-999 aw-upload-tips hidden-xs"> 允许的附件文件类型: jpg,jpeg,png,gif,zip,doc,docx,rar,pdf,psd </span>
										</div>
										  -->
									</div>
								</div>

								<!-- <h3> 添加话题</h3> -->
							<!-- 	<div class="aw-topic-bar" data-type="publish">
									<div class="tag-bar clearfix">
										<span class="topic-tag">
											<a class="text"> // echo urldecode($_GET['topic_title']); </a>
											<a class="close" onclick="$(this).parents('.topic-tag').remove();">
												<i class="icon icon-delete"></i>
											</a>
											<button class="close aw-close" onclick="">×</button></span><input type="hidden" value=" // echo urldecode($_GET['topic_title']); " name="topics[]" />
										</span>
										
										<span class="topic-tag">
											<a class="text"> // echo $val; </a>
											<input type="hidden" value=" // echo $val; " name="topics[]" />
										</span>
										
										<span class="topic-tag">
											<a class="text">  </a>
											<input type="hidden" value=" // echo $val['topic_title']; " name="topics[]" />
										</span>
										

										<span class="aw-edit-topic icon-inverse"><i class="icon icon-edit"></i>  编辑话题 </span>
									</div>
								</div> -->
							
					<!-- 			<div class="aw-edit-topic-box form-inline" style="display: block;"><input class="form-control" id="aw_edit_topic_title" autocomplete="off" placeholder="创建或搜索添加新话题..." type="text"><a class="btn btn-normal btn-success add">添加</a><a class="btn btn-normal btn-gray close-edit">取消</a><div class="aw-dropdown"><p class="title">没有找到相关结果</p><ul class="aw-dropdown-list"></ul></div></div>
							 -->
							</div>
							<div class="mod-footer clearfix">
								<span class="aw-anonymity">
								</span>
								<input type="hidden" name="uid" value="<?php echo $uid; ?>">
								<a class="btn btn-large btn-success cmzPost" table="<?php echo encode('article'); ?>" >确认发起</a>
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- 侧边栏 -->
				<div class="col-sm-12 col-md-3 aw-side-bar hidden-xs">
					<!-- 文章发起指南 -->
					<div class="aw-mod publish-help">
						<div class="mod-head">
							<h3> 文章发起指南</h3>
						</div>
						<div class="mod-body">
							<p><b>• 文章标题 :</b> 请用准确的语言描述您发布的文章思想; </p>
							<p><b>• 文章补充:</b> 详细补充您的文章内容, 并提供一些相关的素材以供参与者更多的了解您所要文章的主题思想; </p>
							<p><b>• 选择话题:</b> 选择一个或者多个合适的话题, 让您发布的文章得到更多有相同兴趣的人参与. 所有人可以在您发布文章之后添加和编辑该文章所属的话题; </p>
						</div>
					</div>
					<!-- end 文章发起指南 -->
				</div>
				<!-- end 侧边栏 -->
		</div>
	</div>
</div>



<?php die; ?>
<div class="aw-footer-wrap">
	<div class="aw-footer">
		Copyright © <?php // echo date('Y'); // if(get_setting('icp_beian')){ ?><span class="hidden-xs"> - <?php // echo get_setting('icp_beian'); // } ?>, All Rights Reserved</span>

		<span class="hidden-xs">Powered By <a href="http://www.5ihelp.com" target="blank">5ihelp </a>感谢Wecenter提供站点框架支持</span>

		<?php // if (is_mobile(true)) { ?>
			<div class="container">
				<div class="row">
					<p align="center"><?php // _e('版本切换'); ?>: <b><?php // _e('PC 版'); ?></b> | <a href="m/ignore_ua_check-FALSE"><?php // _e('手机版'); ?></a></p>
				</div>
			</div>
		<?php // } ?>
	</div>
</div>

<a class="aw-back-top hidden-xs" href="javascript:;" onclick="$.scrollTo(1, 600, {queue:true});"><i class="icon icon-up"></i></a>



<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"></div>

</body>
</html>
